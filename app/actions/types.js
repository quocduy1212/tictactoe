export const PLACE_MARKER = 'PLACE_MARKER';
export const UNDO = 'UNDO';
export const REDO = 'REDO';
export const EOG = 'EOG';
export const INC_STATS = 'INC_STATS';
export const DESC_STATS = 'DESC_STATS';
export const NEW_GAME = 'NEW_GAME';
export const CHANGE_THEME = 'CHANGE_THEME';

