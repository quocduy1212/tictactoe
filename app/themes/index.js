import dracula from './dracula';
import gruvbox from './gruvbox';

export {
  dracula,
  gruvbox,
};
