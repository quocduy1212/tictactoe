import GameTable from './table.scss';
import GameLayout from './layout.scss';
import GameTableCell from './table-cell.scss';
import StatusBar from './status-bar.scss';
import GameBoard from './board.scss';
import MarkerX from './marker-x.scss';
import MarkerO from './marker-o.scss';

const theme = {
  GameTable,
  GameLayout,
  GameTableCell,
  StatusBar,
  GameBoard,
  MarkerX,
  MarkerO,
};

export default theme;
